/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$urlRouterProvider'
    ];

    function Routes(
        // Angular
        $urlRouterProvider
    ) {
        $urlRouterProvider.otherwise('/');
    }

})();
