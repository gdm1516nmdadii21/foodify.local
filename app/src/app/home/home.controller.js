/**
 * @author    Olivier Parent, Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.home')
        .controller('HomeController', HomeController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    HomeController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Custom
        'Shop'
    ];

    function HomeController(
        // Angular
        $log,
        $scope,
        $http,
        // Custom
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            menu: [
                { label: 'Home'         , uri: '#/'           , state: 'home'        },
                { label: 'Shop'         , uri: '#/shop'       , state: 'shop'        },
                { label: 'Cart'         , uri: '#/cart'       , state: 'cart'        },
                { label: 'Login'        , uri: '#/login'      , state: 'login'       }
            ],
            title: 'Home'
        }

        vm.sales = null;
        vm.popular = null;
        vm.recent = null;
        vm.cart = Shop.cart;

        Shop.sales()
            .success(function(data) {
                vm.sales = data;
            });

        Shop.popular()
            .success(function(data) {
                vm.popular = data;
            });

        Shop.recent()
            .success(function(data) {
                vm.recent = data;
            });
    }

})();
