/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.auth')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('login', {
                cache: false, // false will reload on every visit.
                controller: 'AuthController as vm',
                templateUrl: 'html/auth/login.view.html',
                url: '/login'
            })
            .state('register', {
                cache: false, // false will reload on every visit.
                controller: 'AuthController as vm',
                templateUrl: 'html/auth/register.view.html',
                url: '/register'
            });
    }

})();
