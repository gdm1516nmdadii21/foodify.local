/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.auth')
        .controller('AuthController', AuthController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AuthController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Custom
        'Auth'
    ];

    function AuthController(
        // Angular
        $log,
        $scope,
        $http,
        // Custom
        Auth
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            menu: [
                { label: 'Home'         , uri: '#/'           , state: 'home'        },
                { label: 'Shop'         , uri: '#/shop'       , state: 'shop'        },
                { label: 'Cart'         , uri: '#/cart'       , state: 'cart'        },
                { label: 'Login'        , uri: '#/login'      , state: 'login'       }
            ],
            title: 'Login'
        }

        vm.login = {
            email : null,
            password : null,
            submit : function(){
                if(this.email == null || this.password == null || this.email == 'undefined' || this.password == 'undefined'){
                    console.log('Fill all fields');
                }
                else{
                    Auth.login(this.email, this.password)
                    .success(function(auth) {
                        if(!auth.valid){
                            console.log('Auth not valid')
                        }
                    });
                }
            }
        }

        vm.register = {
            given_name : null,
            family_name : null,
            email : null,
            password : null,
            submit : function(){
                if(
                    this.given_name == null || this.given_name == 'undefined' ||
                    this.family_name == null || this.family_name == 'undefined' ||
                    this.email == null || this.email == 'undefined' ||
                    this.password == null || this.password == 'undefined'
                ){
                    console.log('Fill all fields');
                }
                else
                {
                    Auth.register(this.given_name, this.family_name, this.email, this.password)
                    .success(function(auth) {
                        if(!auth.success){
                            console.log(auth.error);
                        }
                    });
                }
            }
        }

    }

})();
