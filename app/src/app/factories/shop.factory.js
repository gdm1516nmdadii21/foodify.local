/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app').factory('Shop', Shop);

    Shop.$inject = [
        // Angular
        '$http',
        '$httpParamSerializerJQLike',
        // Custom
        'Auth'
    ];

    function Shop($http, $httpParamSerializerJQLike, Auth) {

        /*------------------------------------------*\
                         Properties
        \*------------------------------------------*/
        var cart = {
            count : 0,
            items: [],
            add: function(product){
                cart.items.push(product);
                product.incart = true;
            },
            remove: function(product){
                var index = cart.items.indexOf(product);
                if(index >= 0)cart.items.splice(index, 1);
            },
            total: function(){},
            checkout: function(){
                /**
                 * Object to send to laravel
                 */
                var request = {
                    customer_id : Auth.user().id,
                    items : cart.items
                };

                if(cart.items.length > 0){
                    var response = $http({
                        method: 'POST',
                        url: 'http://www.foodify.local/api/v1/checkout',
                        headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                        data: $httpParamSerializerJQLike(request)
                    })
                    .success(function(data){
                        console.log(data);
                        cart.items = [];
                    })
                    .error(function(){
                        console.log('error!');
                    });
                }
            },
        }


        /*------------------------------------------*\
                           Factory
        \*------------------------------------------*/
        return {
            // get sales
            sales : function() {
                return $http.get('http://www.foodify.local/api/v1/sales');
            },

            // get popular
            popular : function() {
                return $http.get('http://www.foodify.local/api/v1/popular');
            },

            // get recent
            recent : function() {
                return $http.get('http://www.foodify.local/api/v1/recent');
            },

            category : function(id){
                return $http.get('http://www.foodify.local/api/v1/category/' + id);
            },

            product : function(id){
                return $http.get('http://www.foodify.local/api/v1/product/' + id);
            },


            cart : function(){
                return cart;
            },


        }

    }

})();
