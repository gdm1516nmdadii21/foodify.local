/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app').factory('Auth', Auth);

    Auth.$inject = [
        // Angular
        '$http',
        '$httpParamSerializerJQLike',
        '$state'
    ];

    function Auth($http, $httpParamSerializerJQLike, $state) {

        /*------------------------------------------*\
                          Properties
        \*------------------------------------------*/
        var authenticated = false;
        var user = null;

        /*------------------------------------------*\
                           Factory
        \*------------------------------------------*/
        return {

            login: function(email, password){

                /**
                 * Object to send to laravel
                 */
                var request = {
                    email: email,
                    password: password
                };

                /**
                 * Post the object to laravel
                 */
                var response = $http({
                    method: 'POST',
                    url: 'http://www.foodify.local/api/v1/auth/',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $httpParamSerializerJQLike(request)
                })
                .success(function(auth){
                    /**
                     * We get an object back
                     *
                     * Valid: true if credentials match
                     * User: the user if valid
                     */
                    if(auth.valid){
                        authenticated = true;
                        user = auth.user;
                        $state.go('account');
                    }
                })
                .error(function(){
                    console.log('error!');
                });

                return response;
            },

            register: function(given_name, family_name, email, password){

                /**
                 * Object to send to laravel
                 */
                var request = {
                    given_name: given_name,
                    family_name: family_name,
                    email: email,
                    password: password
                };

                /**
                 * Post the object to laravel
                 */
                var response = $http({
                    method: 'POST',
                    url: 'http://www.foodify.local/api/v1/auth/register',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $httpParamSerializerJQLike(request)
                })
                .success(function(auth){
                    /**
                     * We get an object back
                     *
                     * Valid: true if credentials match
                     * User: the user if valid
                     */
                    if(auth.success){
                        authenticated = true;
                        user = auth.user;
                        $state.go('account');
                    }
                })
                .error(function(){
                    console.log('error!');
                });

                return response;
            },

            logout: function(){
                authenticated = false;
                user = null;
                $state.go('home');
            },

            authenticated: function(){
                return authenticated;
            },

            user: function(){
                return user;
            }
        }

    }

})();
