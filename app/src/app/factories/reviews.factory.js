/**
 * Created by jefroosens on 9/05/16.
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app').factory('Reviews', Reviews);

    Reviews.$inject = [
        // Angular
        '$resource'
    ];

    function Reviews($resource) {
        return $resource('http://www.foodify.local/api/v1/reviews/:id', { id: '@_id' },{
            save:{
                method: 'POST',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' }
            },
            update: {
                method: 'PUT' // this method issues a POST request
            }
        });
    }


})();
///**/