/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.shop')
        .controller('ShopController', ShopController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    ShopController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Custom
        'Shop'
    ];

    function ShopController(
        // Angular
        $log,
        $scope,
        $http,
        // Custom
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Shop'
        }

        vm.ontbijt = null;
        vm.lunch = null;
        vm.diner = null;
        vm.dessert = null;
        vm.cart = Shop.cart;

        Shop.category(1)
            .success(function(category) {
                vm.ontbijt = category.products;
            });

        Shop.category(2)
            .success(function(category) {
                vm.lunch = category.products;
            });

        Shop.category(3)
            .success(function(category) {
                vm.diner = category.products;
            });

        Shop.category(4)
            .success(function(category) {
                vm.dessert = category.products;
            });

    }

})();
