/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.shop')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('shop', {
                cache: false, // false will reload on every visit.
                controller: 'ShopController as vm',
                templateUrl: 'html/shop/shop.view.html',
                url: '/shop'
            })
            .state('product', {
                cache: false, // false will reload on every visit.
                controller: 'ProductController as vm',
                templateUrl: 'html/shop/product.partial.html',
                url: '/product/{product_id:int}'
            })
        ;
    }

})();
