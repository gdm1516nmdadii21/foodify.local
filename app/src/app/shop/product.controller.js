/**
 * Created by jefroosens on 9/05/16.
 */

;(function () {
    'use strict';

    angular.module('app.product')
        .controller('ProductController', ProductController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    ProductController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        '$httpParamSerializerJQLike',
        '$state',
        // Custom
        'Reviews',
        'Shop',
        'Auth'
    ];

    function ProductController(
        // Angular
        $log,
        $scope,
        $http,
        $httpParamSerializerJQLike,
        $state,
        // Custom
        Reviews,
        Shop,
        Auth
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Product'
        }


        vm.reviews = null;

        Shop.product($state.params.product_id)
            .success(function (product){
                vm.product = product;
            });

        vm.cart = Shop.cart;

        vm.add_review ={
            content : null,
            rating : null,
            product: null,
            submit : function(){
                console.log(this.rating);
                if(
                    this.content === null || this.content === 'undefined' ||
                    this.rating === null || this.rating === 'undefined'
                ){
                    console.log('Fill all fields');
                }
                else
                {
                    var review =  new Reviews();
                    review.content = this.content;
                    review.rating = this.rating;
                    review.product = vm.product;
                    if(Auth.authenticated() == false){
                        $state.go('login');
                    }
                    else {
                        review.user = Auth.user();
                    }
                    console.log(review);
                    Reviews.save($httpParamSerializerJQLike(review) ,function(data){
                        console.log(data);
                        if(!review.success) {
                            console.log(review.error);
                            Shop.product($state.params.product_id)
                                .success(function (product){
                                    vm.product = product;
                                });
                            vm.add_review.content = null;
                            vm.add_review.rating = null;

                        }
                    });
                }
            }
        }







    }

})();
