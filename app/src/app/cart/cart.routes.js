/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.cart')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('cart', {
                cache: false, // false will reload on every visit.
                controller: 'CartController as vm',
                templateUrl: 'html/cart/cart.view.html',
                url: '/cart'
            });
    }

})();
