/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.cart')
        .controller('CartController', CartController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    CartController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        '$state',
        // Custom
        'Shop',
        'Auth'
    ];

    function CartController(
        // Angular
        $log,
        $scope,
        $http,
        $state,
        // Custom
        Shop,
        Auth
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            menu: [
                { label: 'Home'         , uri: '#/'           , state: 'home'        },
                { label: 'Shop'         , uri: '#/shop'       , state: 'shop'        },
                { label: 'Cart'         , uri: '#/cart'       , state: 'cart'        },
                { label: 'Login'        , uri: '#/login'      , state: 'login'       }
            ],
            title: 'Shop'
        }

        vm.cart = Shop.cart;
        vm.checkout = function(){
            if( Auth.authenticated() == false ){
              $state.go('login');
            }
            else{
                vm.cart().checkout();
            }
        };

    }

})();
