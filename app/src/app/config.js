
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .config(Config);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Config.$inject = [
        // Angular
        '$compileProvider',
        // Angular Material Design
        '$mdThemingProvider'
    ];

    function Config(
        // Angular
        $compileProvider,
        // Angular Material Design
        $mdThemingProvider
    ) {
        var debug = true; // Set to `false` for production
        $compileProvider.debugInfoEnabled(debug);

        var colour = {
            amber: 'amber',
            blue: 'blue',
            blueGrey: 'blue-grey',
            brown: 'brown',
            cyan: 'cyan',
            deepOrange: 'deep-orange',
            deepPurple: 'deep-purple',
            green: 'green',
            grey: 'grey',
            indigo: 'indigo',
            lightBlue: 'light-blue',
            lightGreen: 'light-green',
            lime: 'lime',
            orange: 'orange',
            pink: 'pink',
            purple: 'purple',
            red: 'red',
            teal: 'teal',
            yellow: 'yellow'
        };

        $mdThemingProvider.theme('default')
            // .dark()
            .primaryPalette(colour.blue)
            // .accentPalette(colour.lime)
            // .warnPalette(colour.pink)
        ;
        $mdThemingProvider.theme('alternative')
            .dark()
            .primaryPalette(colour.blue)
            .accentPalette(colour.lime)
            .warnPalette(colour.pink)
        ;
    }
})();
