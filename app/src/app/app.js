/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app', [
        // Angular Modules
        // ---------------
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',
        'ui.router',

        // Modules
        // -------
        'app.account',
        'app.auth',
        'app.cart',
        'app.home',
        'app.shop',
        'app.product'
    ]);

    angular.module('app.account', []);
    angular.module('app.auth', []);
    angular.module('app.cart', []);
    angular.module('app.home', []);
    angular.module('app.shop', []);
    angular.module('app.product', []);
})();
