/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .controller('AppController', AppController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AppController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Custom
        'Shop',
        'Auth'
    ];

    function AppController(
        // AppController
        $log,
        $scope,
        $http,
        // Custom
        Shop,
        Auth
    ) {

        // ViewModel
        // =========
        var app = this;

        // User Interface
        // --------------
        app.$$ui = {
            menu: [
                { label: 'Home'         , uri: '#/'           , state: 'home'        },
                { label: 'Shop'         , uri: '#/shop'       , state: 'shop'        },
                { label: 'Cart'         , uri: '#/cart'       , state: 'cart'        },
                { label: 'Login'        , uri: '#/login'      , state: 'login'       },
                { label: 'Register'     , uri: '#/register'   , state: 'register'    },
                { label: 'Account'      , uri: '#/account'    , state: 'account'     }
            ]
        }

        app.islogged = Auth.authenticated;
        app.cart = Shop.cart;

    }

})();
