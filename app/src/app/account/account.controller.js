/**
 * @author    Maxim Vanhove
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.account')
        .controller('AccountController', AccountController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AccountController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        '$state',
        // Custom
        'Shop',
        'Auth'
    ];
    // TODO NG-inject - package json
    function AccountController(
        // Angular
        $log,
        $scope,
        $http,
        $state,
        // Custom
        Shop,
        Auth
    ) {

        if( Auth.authenticated() == false ){
          $state.go('login');
        }

        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Account',
            user: Auth.user
        }

        vm.user = Auth.user;
        vm.cart = Shop.cart;

        console.log(Auth.user());
    }

})();
