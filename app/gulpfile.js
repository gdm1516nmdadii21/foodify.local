/**
 * Created by jefroosens on 23/03/16.
 */
(() => {
    'use strict';

let fs = require('fs');

fs.readdirSync('./gulpfile/tasks')
    .filter(filename => {
    return filename.match(/\.js$/i);
})
.map(filename => {
    require(`./gulpfile/tasks/${filename}`);
});

})();