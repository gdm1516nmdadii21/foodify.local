/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp  = require('gulp'),
        watch = require('gulp-watch');

    gulp.task('watch', [
        'watch:templates',
        'scripts:watch',
        'styles:watch',
        'markup:watch'
    ]);

    gulp.task('watch:templates', () => {
        gulp.src(CONFIG.dir.templates.src)
            .pipe(watch(CONFIG.dir.templates.src))
            .pipe(gulp.dest(CONFIG.dir.templates.dest));
    });

})();
