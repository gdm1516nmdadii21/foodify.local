/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp   = require('gulp'),
        sass   = require('gulp-sass'),
        postcss = require('gulp-postcss'),
        autoprefixer = require('autoprefixer');

    gulp.task('styles', [
        'styles:app'
    ]);

    gulp.task('styles:app', () => {
        var processors = [
            autoprefixer({browsers: ['last 1 version']})
        ];
        gulp.src('./src/css/*.scss')
            .pipe(sass(CONFIG.sass).on('error', sass.logError))
            .pipe(postcss(processors))
            .pipe(gulp.dest('./dist/css'));
    });

    gulp.task('styles:watch', function () {
      gulp.watch('./src/css/**/*.scss', ['styles:app']);
    });

})();
