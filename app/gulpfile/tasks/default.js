/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp = require('gulp'),
        exec = require('child_process').exec;

    gulp.task('default', [
        'default:remove',
        'src:icons',
        'src:fonts',
        'vendor',
        'markup',
        'scripts',
        'styles'
    ]);

    gulp.task('src:icons', () => {
        gulp.src('./src/icons/**/*')
            .pipe(gulp.dest('./dist/icons/'));
    });

    gulp.task('src:fonts', () => {
        gulp.src('./src/fonts/**/*')
            .pipe(gulp.dest('./dist/fonts/'));
    });

    gulp.task('default:remove', () => {
        exec(`rm -rf ${CONFIG.dir.dest}`);
    });

})();
