/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp   = require('gulp');

    gulp.task('markup', [
        'markup:app',
        'markup:templates'
    ]);

    gulp.task('markup:app', () => {
        gulp.src('./src/*.html')
            .pipe(gulp.dest('./dist/'));
    });

    gulp.task('markup:templates', () => {
        gulp.src(CONFIG.dir.templates.src)
            .pipe(gulp.dest(CONFIG.dir.templates.dest));
    });

    gulp.task('markup:watch', function () {
      gulp.watch('./src/*.html', ['markup:app']);
    });

})();
