
/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp   = require('gulp'),
        concat = require('gulp-concat');

    gulp.task('vendor', [
        'vendor:angular',
        'vendor:faker',
        'vendor:lodash'
    ]);

    gulp.task('vendor:angular', () => {
        return gulp // JavaScript
            .src([
                './node_modules/angular/angular.js',
                './node_modules/angular-animate/angular-animate.js',
                './node_modules/angular-aria/angular-aria.js',
                './node_modules/angular-material/angular-material.js',
                './node_modules/angular-messages/angular-messages.js',
                './node_modules/angular-resource/angular-resource.js',
                './node_modules/angular-ui-router/release/angular-ui-router.js'
            ])
            .pipe(concat('angular.js'))
            .pipe(gulp.dest('./dist/vendor/angular/'));
    });

    gulp.task('vendor:faker', () => {
        return gulp // JavaScript
            .src('./node_modules/faker/build/build/faker.min.js')
            .pipe(gulp.dest('./dist/vendor/faker/'));
    });

    gulp.task('vendor:lodash', () => {
        return gulp // JavaScript
            .src('./node_modules/lodash/lodash.min.js')
            .pipe(gulp.dest('./dist/vendor/lodash/'));
    });

})();
