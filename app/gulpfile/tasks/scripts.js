/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp   = require('gulp'),
        concat = require('gulp-concat');

    gulp.task('scripts', [
        'scripts:app'
    ]);

    gulp.task('scripts:app', () => {
        return gulp.src('./src/app/**/*.js')
            .pipe(concat('app.js'))
            .pipe(gulp.dest('./dist/js/'));
    });

    gulp.task('scripts:watch', function () {
      gulp.watch('./src/app/**/*', ['scripts:app']);
    });

})();
