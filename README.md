New Media Design & Development II
==================================

	foodify.local/
	├── docs/
	│   ├── academische_poster.pdf
	│   ├── checklist.md
	│   ├── presentatie.pdf
	│   ├── productiedossier.pdf
	│   ├── timesheet_jef_Roosens.xslx
	│   └── timesheet_maxim_vanhove.xslx
	├── www/
	└── README.md
	
## Info

- Contributors: **Jef Roosens & Maxim Vanhove**
- Opleidingsonderdeel: **New Media Design & Development II**
- Academiejaar: **2015-2016**
- Opleiding: **Bachelor in de grafische en digitale media**
- Afstudeerrichting: **Multimediaproductie**
- Keuzeoptie: **proDEV**
- Opleidingsinstelling: **Arteveldehogeschool**

## Deploy

### Clone

```
$ cd ~/Code/
```

```
$ git clone https://MaximVanhove@bitbucket.org/gdm1516nmdadii21/foodify.local.git
```

### Install

```
$ cd ~/Code/foodify.local/
```

```
$ composer install
```

```
$ cd ~/Code/foodify.local/www/
```

```
$ composer install
```

### Create Artestead.yaml

Create a Artestead.yaml file in ~/Code/foodify.local/ and change the mappings

Or if you don't have a Artestead.yaml yet, let artestead create one

```
$ cd ~/Code/foodify.local/
```

```
$ artestead make --type laravel
```

### Create an .env file from the example file

```
$ cd ~/Code/foodify.local/www/
```

```
$ cp .env.example .env
```

### Start machine

```
$ vagrant up
```

```
$ vagrant ssh
```

### Generate key

```
$ cd ~/Code/foodify.local/www/
```

```
$ php artisan key:generate
```

### Create database

```
$ cd ~/Code/foodify.local/www/
```

```
$ artisan artevelde:database:user
```

```
$ artisan artevelde:database:init
```

```
$ artisan migrate --seed
```
Als je een foutmelding krijgt:

```
$ composer dump-autoload
```

### Done

You can now go to http://www.foodify.local

## Update

```
$ composer selfupdate
```

```
$ composer g update
```

```
$ cd ~/Code/foodify.local/
```

```
$ rm Artestead.yaml aliases.sh
```

```
$ artestead make --type laravel
```

```
$ vagrant up --provision
```