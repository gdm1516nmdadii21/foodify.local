<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'price', 'promoted', 'image_url','promoted_price', 'category_id'
    ];

    protected $hidden = [
        'category_id', 'created_at', 'updated_at', 'deleted_at'
    ];

    // Relationships
    // =============
    /**
     * Many-to-one
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Many-to-one
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    /**
     * Many-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lovers()
    {
        return $this->belongsToMany(User::class, 'user_id', 'id');
    }
}
