<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Purchase extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'image_url', 'price', 'user_id'
    ];

    // Relationships
    // =============
    /**
     * Many-to-one
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function costumer()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
