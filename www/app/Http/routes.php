<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    \Debugbar::disable();
    Route::auth();

    Route::get('/', 'BackofficeController@index');
    Route::get('/backoffice', 'BackofficeController@index');
    Route::get('/shop', 'CategoryController@index');
    Route::resource('/product', 'ProductController');
    Route::get('product/create/{categoryId}', 'ProductController@create');
    Route::resource('/purchase', 'PurchaseController');
    Route::put('purchase/status/{id}', 'PurchaseController@status')->name('purchase.status');
    Route::resource('/user', 'UserController');
    Route::resource('/review', 'ReviewController');

});

Route::group(['prefix' => 'api/v1', 'middleware' => ['cors']], function () {
    \Debugbar::disable();
    Route::get('categories', 'ApiController@getCategories');
    Route::get('category/{id}', 'ApiController@getCategory');
    Route::get('product/{id}', 'ApiController@getProduct');
    Route::get('popular', 'ApiController@getPopular');
    Route::get('recent', 'ApiController@getRecent');
    Route::get('sales', 'ApiController@getSales');
    Route::get('allsales', 'ApiController@getAllSales');
    Route::post('checkout', 'ApiController@checkout');
    Route::post('auth', 'ApiController@auth');
    Route::post('auth/register', 'ApiController@register');
    Route::resource('reviews', 'Api\ReviewController');

});
