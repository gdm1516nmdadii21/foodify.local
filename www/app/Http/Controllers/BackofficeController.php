<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Purchase;
use App\Models\Category;
use App\Models\Product;
use App\Models\Review;
use App\User;
use Carbon\Carbon;

class BackofficeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchased = Purchase::orderBy('created_at','desc')->orderBy('id','desc')->with('costumer')->take(5)->get();
        $users = User::orderBy('created_at','desc')->take(5)->get();
        $mostPopularProducts = Product::orderBy('buyed','desc')->take(10)->get();
        $leastPopularProducts = Product::orderBy('buyed','asc')->take(10)->get();
        $reviews = Review::OrderBy('created_at', 'asc')->take(5)->get();

        $earnings = array();
        $today = Carbon::today();

        /**
         * Loop from 6 days back to today
         */
        for ($i=6; $i >= 0 ; $i--) {

            $day = new Carbon($today);
            $day->subDays($i);

            $dayafter = new Carbon($day);
            $dayafter->addDay();

            $total = 0;
            $purchases = Purchase::where('created_at', '>', $day)->where('created_at', '<', $dayafter)->get();

            /**
             * Get all earnings for this day
             */
            foreach ($purchases as $key => $purchase) {
                $total += $purchase->price;
            }

            switch ($i) {
                case 0:
                    $key = 'Vandaag';
                    break;

                case 1:
                    $key = 'Gisteren';
                    break;

                default:
                    $key = $day->formatLocalized('%d %b');
                    break;
            }
            $earnings[$key] = $total;
        }

        return view('backoffice.dashboard')
            ->with([
                'purchased' => $purchased,
                'users' => $users,
                'reviews' => $reviews,
                'mostPopularProducts' => $mostPopularProducts,
                'leastPopularProducts' => $leastPopularProducts,
                'earnings' => $earnings
            ]);
    }
}
