<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
		 * Get all users
		 */
        $users = User::get();

        /**
		 * Load the index view and pass the users
		 */
        return view('user.index')
            ->with(['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**
        * Load the create view
        */
       return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Server Validation
         */
        $this->validate($request, [
	        'name' => 'required',
	        'email' => 'required'
	    ]);

        $input = $request->all();

        $user = User::create($input);

        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
         * Get the user
         */
		$user = User::findOrFail($id);

        /**
         * Load the view and pass the user
         */
		return view('user.edit')->with(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Server Validation
         */
        $this->validate($request, [
	        'name' => 'required',
	        'email' => 'required'
	    ]);

        /**
         * Get the user
         */
		$user = User::findOrFail($id);

        $input = $request->all();

        $user->fill($input)->save();

        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**
         * Get the user
         */
		$user = User::findOrFail($id);

		$user->delete();

		return redirect('user');
    }
}
