<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Purchase;

class ApiController extends Controller
{
    /**
     * Get the products from a category
     *
     * @return JSON
     */
    public function getCategories()
    {
        /**
         * Find the refered category with it's products
         */
        $categories = Category::with('products')->get();

        return $categories->toArray();
    }

    /**
     * Get the products from a category
     *
     * @return JSON
     */
    public function getCategory($id)
    {
        /**
         * Find the refered category with it's products
         */
        $category = Category::with('products')->findOrFail($id);

        return $category->toArray();
    }

    /**
     * Get the product by id
     *
     * @return JSON
     */
    public function GetProduct($id)
    {
        /**
         * Find the refered product
         */
        $product = product::with('reviews')->with('reviews.author')->findOrFail($id);

        return $product;
    }

    /**
     * Get the popular products
     *
     * @return JSON
     */
    public function getPopular()
    {
        /**
         * Get 4 most popular products by buy count
         */
        $products = Product::orderBy('buyed', 'desc')->take(4)->get();

        return $products->toArray();
    }

    /**
     * Get the recent products
     *
     * @return JSON
     */
    public function getRecent()
    {
        /**
         * Get 4 most recent products by created_at
         */
        $products = Product::orderBy('created_at', 'desc')->take(4)->get();

        return $products->toArray();
    }

    /**
     * Get the sales
     *
     * @return JSON
     */
    public function getSales()
    {
        /**
         * Get 4 sales ordered by updated_at
         */
        $products = Product::where('promoted', 1)->orderBy('updated_at', 'desc')->take(4)->get();

        return $products->toArray();
    }

    /**
     * Get all sales
     *
     * @return JSON
     */
    public function getAllSales()
    {
        /**
         * Get all sales ordered by updated_at
         */
        $products = Product::where('promoted', 1)->orderBy('updated_at', 'desc')->get();

        return $products->toArray();
    }

    /**
    * Check out. Angular post to laravel
    *
    * @return JSON
    */
    public function checkout(Request $request)
 	{
        /**
         * Get the data from angular
         */
        $input = $request->all();

        $id = $input['customer_id'];

        /**
         * Create a purchase for each item in the cart
         */
        foreach ($input['items'] as $key => $item) {
            /**
             * Get the product
             */
            $product = Product::findOrFail($item['id']);

            $price = $product->price;
            if($product->promoted){
                $price = $product->promoted_price;
            }

            Purchase::create([
                'user_id'   => $id,
                'title'     => $product->title,
                'content'   => $product->content,
                'image_url' => $product->image_url,
                'price'     => $price
            ]);
        }

        return array(
            "success" => true
        );
    }

    /**
     * Validate auth
     *
     * @return JSON
     */
    public function auth(Request $request)
    {
        /**
         * Get the data from angular
         */
        $input = $request->all();
        $email = $input['email'];
        $password = $input['password'];

        /**
         * Get the user
         */
        $user = User::where('email', $email)->first();

        /**
         * If email is not found
         */
        if(!isset($user) || $user == null){
            return array(
                "valid" => false,
            );
        }

        return array(
            "user"  => $user,
            "valid" => Hash::check($password, $user->password),
        );
    }

    /**
     * Register a new user
     *
     * @return JSON
     */
    public function register(Request $request){
        /**
         * Get the data from angular
         */
        $input = $request->all();
        $givenName = $input['given_name'];
        $familyName = $input['family_name'];
        $email = $input['email'];
        $password = $input['password'];

        $exists = User::where('email', $email)->first();

        if (isset($exists)){
            return array(
                "user"  => null,
                "success" => false,
                "error" => 'Email is already in use',
            );
        }

        /**
         * Create the user
         */
        $user = User::create([
            'email' => $email,
            'name' => $givenName . ' ' . $familyName,
            'given_name' => $givenName,
            'family_name' => $familyName,
            'password' => Hash::make($password),
        ]);

        /**
         * If the user creation was succesful
         */
        if(isset($user) && $user != null){
            $response = array(
                "user"  => $user,
                "success" => true,
            );
        }
        else{
            $response = array(
                "user"  => null,
                "success" => false,
                "error" => 'User could not be created',
            );
        }

        return $response;

    }
}
