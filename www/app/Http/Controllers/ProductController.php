<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('shop/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoryId = -1)
    {
        /**
         * Get the first category if it's not supplied
         */
        if($categoryId == -1){
            $categoryId = Category::first()->id;
        }

        /**
         * Get all the categories
         */
		$categories = Category::lists('name', 'id');

		/**
		 * Load the view and pass the categories
		 */
		return view('product.create')
			->with(['categories' => $categories, 'categoryId' => $categoryId]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Validation fields are filled
         */
	    $this->validate($request, [
	        'title' => 'required',
	        'price' => 'required',
            'image' => 'required'
	    ]);

        $image = '';

        /**
         * Add Picture to product
         */
        if ($request->file('image')->isValid()) {
            $file = $request->file('image');
            $destinationPath = public_path(). DIRECTORY_SEPARATOR . 'uploads';
            $fileName = sha1_file($file->getRealPath()) . '.' . $file->guessExtension();
            $file->move($destinationPath, $fileName);
            $image = '/uploads/' . $fileName;
        }

        /**
         * Validation if promoted price is less than the original price
         */
        if($request->get('price') <= $request->get('promoted_price')){
            $error = "De promo prijs moet lager zijn dan de originele prijs";
            return redirect('product/'.$product->id.'/edit')->with('error', $error);
        }

	    $input = $request->all();

        /**
         * Bug fix where laravel can't bind the checkbox when not checked
         */
        if(isset($input['promoted'])){
            $input['promoted'] = 1;
        }
        else {
            $input['promoted'] = 0;
        }

        /**
         * Add the image path
         */
        $input['image_url'] = $image;

        /**
         * Add the category foreign key to the input
         */
		$categoryId = $request->get('category');
		$category = Category::findOrFail($categoryId);
        $input['category_id'] = $category->id;

        $product = Product::create($input);
		$category->products()->save($product);

	    return redirect('shop');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
         * Get all the categories
         */
        $categories = Category::lists('name', 'id');

        /**
         * Get the product
         */
		$product = Product::findOrFail($id);

        /**
         * Load the view and pass the product and categories
         */
		return view('product.edit')->with(['product' => $product,'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Get the current product
         */
        $product = Product::findOrFail($id);

	    $this->validate($request, [
	        'title' => 'required',
	        'price' => 'required'
	    ]);

        if($request->get('price') <= $request->get('promoted_price')){
            $error = "De promo prijs moet lager zijn dan de originele prijs";
            return redirect('product/'.$product->id.'/edit')->with('error', $error);
        }

	    $input = $request->all();

        $image = '';

        /**
         * Edit Picture to product
         */
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $file = $request->file('image');
            $destinationPath = public_path(). DIRECTORY_SEPARATOR . 'uploads';
            $fileName = sha1_file($file->getRealPath()) . '.' . $file->guessExtension();
            $file->move($destinationPath, $fileName);
            $image = '/uploads/' . $fileName;
        }

        /*
         * Bug fix where laravel can't bind the checkbox when not checked
         */
        if(isset($input['promoted'])){
            $input['promoted'] = 1;
        }
        else {
            $input['promoted'] = 0;
        }

		$categoryId = $request->get('category');
		$category = Category::findOrFail($categoryId);

	    $product->fill($input)->save();
		$category->products()->save($product);

	    return redirect('shop');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
		$product->delete();

		return redirect('shop');
    }
}
