<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Ontbijt',
            'description' => 'Producten voor het ontbijt',
        ]);

        Category::create([
            'name' => 'Lunch',
            'description' => 'Producten voor de lunch',
        ]);

        Category::create([
            'name' => 'Diner',
            'description' => 'Producten voor het diner',
        ]);

        Category::create([
            'name' => 'Dessert',
            'description' => 'Producten voor het dessert',
        ]);
    }
}
