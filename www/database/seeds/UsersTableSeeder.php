<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'maxim.vanhove@student.arteveldehs.be',
            'name' => 'Maxim Vanhove',
            'password' => Hash::make('wachtwoord'),
            'given_name' => 'Maxim',
            'family_name' => 'Vanhove',
        ]);

        User::create([
            'email' => 'jef.roosens@student.arteveldehs.be',
            'name' => 'Jef Roosens',
            'password' => Hash::make('wachtwoord'),
            'given_name' => 'Jef',
            'family_name' => 'Roosens',
        ]);

        // Faker
        // -----
        factory(User::class, DatabaseSeeder::AMOUNT['DEFAULT'])->create();
    }
}
