<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{

    const MODEL = 'review';
    const TABLE = self::MODEL.'s';
    const PK = 'id';
    const FK = self::MODEL.'_'.self::PK;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE, function (Blueprint $table) {
            // Primary Key
            $table->increments(self::PK);

            // Foraign Keys
            $table->unsignedInteger(CreateProductsTable::FK);
            $table->foreign(CreateProductsTable::FK)->references(CreateProductsTable::PK)->on(CreateProductsTable::TABLE)->onDelete('cascade');

            $table->unsignedInteger(CreateUsersTable::FK);
            $table->foreign(CreateUsersTable::FK)->references(CreateUsersTable::PK)->on(CreateUsersTable::TABLE)->onDelete('cascade');

            // Data
            $table->text('content');
            $table->integer('rating');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
