<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    const MODEL = 'product';
    const TABLE = self::MODEL.'s';
    const PK = 'id';
    const FK = self::MODEL.'_'.self::PK;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE, function (Blueprint $table) {
            // Primary Key
            $table->increments(self::PK);

            // Foreign Keys
            $table->unsignedInteger(CreateCategoriesTable::FK);
            $table->foreign(CreateCategoriesTable::FK)->references(CreateCategoriesTable::PK)->on(CreateCategoriesTable::TABLE)->onDelete('cascade');

            // Data
            $table->string('title');
            $table->text('content');
            $table->string('image_url');
            $table->double('price', 7, 2); //7 digits in total and 2 after the decimal point.
			$table->double('promoted_price', 7, 2); //7 digits in total and 2 after the decimal point.
			$table->boolean('promoted');
            $table->integer('buyed')->unsigned(); // Count of buys

            // Meta Data
            $table->timestamps(); // 'created_at', 'updated_at'
            $table->softDeletes(); // 'deleted_at'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE);
    }
}
