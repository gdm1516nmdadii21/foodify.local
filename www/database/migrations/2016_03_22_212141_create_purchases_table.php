<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    const MODEL = 'purchase';
    const TABLE = self::MODEL.'s';
    const PK = 'id';
    const FK = self::MODEL.'_'.self::PK;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            // Primary Key
            $table->increments(self::PK);

            // Foreign Keys
            $table->unsignedInteger(CreateUsersTable::FK);
            $table->foreign(CreateUsersTable::FK)->references(CreateUsersTable::PK)->on(CreateUsersTable::TABLE)->onDelete('cascade');

            // Data
            // TODO: Add Product foreign key and remove title, content, image_url. Keep the price.
            $table->string('title');
            $table->text('content');
            $table->string('image_url');
            $table->double('price', 7, 2); //7 digits in total and 2 after the decimal point.
            $table->integer('status');

            // Meta Data
            $table->timestamps(); // 'created_at', 'updated_at'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases');
    }
}
