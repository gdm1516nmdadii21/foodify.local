<?php

use App\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Review;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker $faker) : array {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => Hash::make($faker->password($minLength = 6, $maxLength = 20)),
        'remember_token' => str_random(10),
        'given_name' => $faker->firstName,
        'family_name' => $faker->lastName,
    ];
});

$factory->define(Product::class, function (Faker $faker) : array {
    $price = $faker->randomFloat($nbMaxDecimals = 2, $min = 20, $max = 200); // 48.89, 129.34
    $promoted = false;
    $promoted_price = 0;
    if(rand(0, 3) == 0){
        $promoted = true;
        $promoted_price = ($price - $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 15));
    }
    return [
        CreateCategoriesTable::FK => Category::all()->random()->{CreateCategoriesTable::PK},
        'title' => $faker->sentence($nbwords = 3),
        'content' => $faker->paragraph($nbSentences = rand(1, 10)),
        'image_url' => $faker->imageUrl($width = 300, $height = 300, 'food'),
        'price' => $price,
        'promoted' => $promoted,
        'promoted_price' => $promoted_price,
        'buyed' => $faker->numberBetween($min = 10, $max = 999) // 8567
    ];
});

$factory->define(Review::class, function (Faker $faker) : array{
    return[
        CreateProductsTable::FK => Product::all()->random()->{CreateProductsTable::PK},
        CreateUsersTable::FK => User::all()->random()->{CreateUsersTable::PK},
        'content' => $faker->paragraph($nbSentences = rand(1, 10)),
        'rating' => $faker->numberBetween($min = 0, $max = 5)
    ];
});