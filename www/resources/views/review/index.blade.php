@extends('layouts.backoffice')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Backoffice</a></li>
                <li class="active">Reviews</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if ($reviews == null || count($reviews) < 1)
                <p>
                    Er zijn nog geen aankopen!
                </p>
            @else
                <table class="table">
                    <tbody>
                    <tr>
                        <th>Klant</th>
                        <th>Product</th>
                        <th>Content</th>
                        <th>Rating</th>
                        <th>Date</th>
                        <th>Actions</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach ($reviews as $key => $review)
                        <tr>
                            <td>{{$review->author->name}}</td>
                            <td>{{$review->product->title}}</td>
                            <td>{{$review->content}}</td>
                            <td>{{$review->rating}}</td>
                            <td>{{$review->created_at}}</td>
                            <td>
                                {!! Form::open(['method' => 'DELETE','route' => ['review.destroy', $review->id], 'class' => 'form-delete']) !!}
                                <a class="btn btn-danger modal-button" data-product-id="{{ $review->id }}" data-toggle="modal" href="#modal-confirm"><i class="fa fa-trash-o"></i></a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <!-- Modals -->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Verwijderen</h4>
                </div>
                <div class="modal-body">

                    Ben je zeker dat je deze gebruiker wilt verwijderen?

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Terug</button>
                    <button class="btn btn-warning modal-confirm-button" type="button"> Verwijderen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var activeForm;

        $('.modal-button').click(handleModal);
        $('.modal-confirm-button').click(handleConfirm);

        function handleModal(e){
            activeForm = $(this).closest('.form-delete');
        }

        function handleConfirm(e){
            activeForm.submit();
        }
    </script>
@endsection
