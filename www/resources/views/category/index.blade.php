@extends('layouts.backoffice')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Backoffice</a></li>
            <li class="active">Shop</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @if ($categories == null || count($categories) < 1)
            <p>
                Er zijn nog geen categorieën!
            </p>
            <p>
                <a href="{{ url('/product/create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Voeg een product toe</a>
            </p>
        @else
        <section class="panel general">
            <header class="panel-heading tab-bg-dark-navy-blue">
                <ul class="nav nav-tabs">
                    @foreach ($categories as $key => $category)
                    @if($key == 0)
                    <li class="active">
                    @else
                    <li class="">
                    @endif
                        <a data-toggle="tab" href="#category-{{ $category->id }}">{{ $category->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </header>
            <div class="panel-body">
                <div class="tab-content">
                    @foreach ($categories as $key => $category)
                    @if($key == 0)
                    <div id="category-{{ $category->id }}" class="tab-pane active">
                    @else
                    <div id="category-{{ $category->id }}" class="tab-pane">
                    @endif
                        <p>
                            <a href="{{ url('/product/create/'.$category->id) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Voeg een product toe</a>
                        </p>
                        @if(count($category->products) > 0)
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th></th>
                                    <th>Title</th>
                                    <th>Price</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach ($category->products as $key => $product)
                                <tr>
                                    <td><img src="{{$product->image_url}}" alt="{{$product->title}}" style="width:50px;"/></td>
                                    <td>{{$product->title}}</td>
                                    @if($product->promoted)
                                    <td>
                                        <strike>€ {{$product->price}} </strike> <span class="label label-danger">€ {{$product->promoted_price}}</span>
                                    </td>
                                    @else
                                    <td>€ {{$product->price}}</td>
                                    @endif
                                    <td>
                                        {!! Form::open(['method' => 'DELETE','route' => ['product.destroy', $product->id], 'class' => 'form-delete']) !!}
            								<a href="{{ url('/product/'.$product->id.'/edit') }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-danger modal-button" data-product-id="{{ $product->id }}" data-toggle="modal" href="#modal-confirm"><i class="fa fa-trash-o"></i></a>
        							    {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <p>
                            Er zijn nog geen producten in deze categorie
                        </p>
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        @endif
    </div>
</div>

<!-- Modals -->
<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Verwijderen</h4>
            </div>
            <div class="modal-body">

                Ben je zeker dat je dit product wilt verwijderen?

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Terug</button>
                <button class="btn btn-warning modal-confirm-button" type="button"> Verwijderen</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
var activeForm;

$('.modal-button').click(handleModal);
$('.modal-confirm-button').click(handleConfirm);

function handleModal(e){
    activeForm = $(this).closest('.form-delete');
}

function handleConfirm(e){
    activeForm.submit();
}
</script>
@endsection
