@extends('layouts.backoffice')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Backoffice</a></li>
            <li class="active">Dashboard</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <p class="panel-heading">Recent purchases</p>
            <div class="panel-body">
                <div class="col-md-12">
                    @if ($purchased == null || count($purchased) < 1)
                        <p>
                            Er zijn nog geen aankopen!
                        </p>
                    @else
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>User</th>
                            </tr>
                            @foreach ($purchased as $key => $purchase)
                                <tr>
                                    <td>{{$purchase->id}}</td>
                                    <td>{{$purchase->title}}</td>
                                    <td>€{{$purchase->price}}</td>
                                    <td> {{$purchase->costumer->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <p>
                            <a href="{{ url('/purchase') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Meer bekijken</a>
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <p class="panel-heading">Recent reviews</p>
            <div class="panel-body">
                <div class="col-md-12">
                    @if ($reviews == null || count($reviews) < 1)
                        <p>
                            Er zijn nog geen reviews!
                        </p>
                    @else
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>Id</th>
                                <th>Product</th>
                                <th>Rating</th>
                                <th>User</th>
                            </tr>
                            @foreach ($reviews as $key => $review)
                                <tr>
                                    <td>{{$review->id}}</td>
                                    <td>{{$review->product->title}}</td>
                                    <td>{{$review->rating}}</td>
                                    <td>{{$review->author->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <p>
                            <a href="{{ url('/purchase') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Meer bekijken</a>
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <p class="panel-heading">Recent users</p>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        @if ($users == null || count($users) < 1)
                            <p>
                                Er zijn nog geen gebruikers!
                            </p>
                        @else
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                </tr>
                                @foreach ($users as $key => $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>
                                <a href="{{ url('/user') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Meer bekijken</a>
                            </p>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                Earnings
            </header>
            <div class="panel-body">
                <section>
                    <canvas id="linechart" width="600" height="200"></canvas>
                </section>
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <p class="panel-heading">Most popular products</p>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        @if ($mostPopularProducts == null || count($mostPopularProducts) < 1)
                            <p>
                                Er zijn nog geen producten!
                            </p>
                        @else
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>Title</th>
                                    <th>Price</th>
                                </tr>
                                @foreach ($mostPopularProducts as $key => $mostPopularProduct)
                                    <tr>
                                        <td>{{$mostPopularProduct->title}}</td>
                                        @if($mostPopularProduct->promoted)
                                            <td>
                                                <strike>€ {{$mostPopularProduct->price}} </strike> <span class="label label-danger">€ {{$mostPopularProduct->promoted_price}}</span>
                                            </td>
                                        @else
                                            <td>€ {{$mostPopularProduct->price}}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>
                                <a href="{{ url('/shop') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Meer bekijken</a>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <p class="panel-heading">Least popular products</p>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        @if ($leastPopularProducts == null || count($leastPopularProducts) < 1)
                            <p>
                                Er zijn nog geen producten!
                            </p>
                        @else
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>Title</th>
                                    <th>Price</th>
                                </tr>
                                @foreach ($leastPopularProducts as $key => $leastPopularProduct)
                                    <tr>
                                        <td>{{$leastPopularProduct->title}}</td>
                                        @if($leastPopularProduct->promoted)
                                            <td>
                                                <strike>€ {{$leastPopularProduct->price}} </strike> <span class="label label-danger">€ {{$leastPopularProduct->promoted_price}}</span>
                                            </td>
                                        @else
                                            <td>€ {{$leastPopularProduct->price}}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>
                                <a href="{{ url('/shop') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Meer bekijken</a>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    ;(function (){
        "use strict";
        //BAR CHART
        var data = {
            labels: [
                @foreach($earnings as $key => $item)
                "{{ $key }}",
                @endforeach
            ],
            datasets: [
                // {
                //     label: "My First dataset",
                //     fillColor: "rgba(220,220,220,0.2)",
                //     strokeColor: "rgba(220,220,220,1)",
                //     pointColor: "rgba(220,220,220,1)",
                //     pointStrokeColor: "#fff",
                //     pointHighlightFill: "#fff",
                //     pointHighlightStroke: "rgba(220,220,220,1)",
                //     data: [65, 59, 80, 81, 56, 55, 40]
                // },
                {
                    label: "Earnings for last 7 days",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [
                        @foreach($earnings as $key => $item)
                        {{ $item }},
                        @endforeach
                    ]
                }
            ]
        };

        new Chart(document.getElementById("linechart").getContext("2d")).Line(data,{
            responsive : true,
            maintainAspectRatio: false,
        });
    })();

    Chart.defaults.global.responsive = true;
</script>
@endsection
