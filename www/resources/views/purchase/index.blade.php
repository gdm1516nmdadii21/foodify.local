@extends('layouts.backoffice')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Backoffice</a></li>
                <li class="active">Purchases</li>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if ($purchases == null || count($purchases) < 1)
                <p>
                    Er zijn nog geen aankopen!
                </p>
            @else
                <table class="table">
                    <tbody>
                    <tr>
                        <th>Status</th>
                        <th>Titel</th>
                        <th>Prijs</th>
                        <th>Klant</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach ($purchases as $key => $purchase)
                        <tr>
                            <td>
                                @if($purchase->status == 0)
                                <span class="label label-danger">nieuw</span>
                                @elseif($purchase->status == 1)
                                <span class="label label-warning">verpakt</span>
                                @elseif($purchase->status == 2)
                                <span class="label label-primary">onderweg</span>
                                @elseif($purchase->status == 3)
                                <span class="label label-success">geleverd</span>
                                @endif
                            </td>
                            <td>{{$purchase->title}}</td>
                            <td>{{$purchase->price}}</td>
                            <td>{{$purchase->costumer->name}}</td>
                            <td>
                                {!! Form::open(['method' => 'PUT','route' => ['purchase.status', $purchase->id]]) !!}
                                    {{ Form::hidden( 'status', ($purchase->status+1) ) }}
                                    @if($purchase->status == 0)
                                    {!! Form::submit('Opgepikt', ['class' => 'btn btn-primary form-control']) !!}
                                    @elseif($purchase->status == 1)
                                    {!! Form::submit('Verzonden', ['class' => 'btn btn-primary form-control']) !!}
                                    @elseif($purchase->status == 2)
                                    {!! Form::submit('Geleverd', ['class' => 'btn btn-primary form-control']) !!}
                                    @endif
                                {!! Form::close() !!}
                            </td>
                            <td>
                                {!! Form::open(['method' => 'DELETE','route' => ['purchase.destroy', $purchase->id], 'class' => 'form-delete']) !!}
                                <a class="btn btn-danger modal-button" data-product-id="{{ $purchase->id }}" data-toggle="modal" href="#modal-confirm"><i class="fa fa-trash-o"></i></a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <!-- Modals -->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Verwijderen</h4>
                </div>
                <div class="modal-body">

                    Ben je zeker dat je deze gebruiker wilt verwijderen?

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Terug</button>
                    <button class="btn btn-warning modal-confirm-button" type="button"> Verwijderen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var activeForm;

        $('.modal-button').click(handleModal);
        $('.modal-confirm-button').click(handleConfirm);

        function handleModal(e){
            activeForm = $(this).closest('.form-delete');
        }

        function handleConfirm(e){
            activeForm.submit();
        }
    </script>
@endsection
