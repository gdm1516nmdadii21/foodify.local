@extends('layouts.backoffice')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Backoffice</a></li>
            <li><a href="{{ url('/shop') }}">Shop</a></li>
            <li class="active">Product</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @if(null !== Session::get('error'))
        <div class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Er is iet fout!</strong> {{ Session::get('error') }}
        </div>
        @endif

		{!! Form::model($product, [
			'method' => 'PATCH',
			'route' => ['product.update', $product->id]
		]) !!}
		<div class="form-group">
			{!! Form::label('title', 'Titel:') !!}
			{!! Form::text('title',null,['class'=>'form-control']) !!}
		</div>
        <div class="form-group">
			{!! Form::label('content', 'Beschrijving:') !!}
			{!! Form::textarea('content',null,['class'=>'form-control']) !!}
		</div>
			<div class="form-group">
				{!! Form::label('image_url' , 'Afbeelding:', $options = [
                    'class' => 'control-label',
                ]) !!}
				{!! Form::file('image', $options = [
                    'accept' => '.jpg,.jpeg,.png,.svg,image/*', // @link http://www.iana.org/assignments/media-types/media-types.xhtml#image
                ]) !!}
			</div>
		<div class="form-group">
			{!! Form::label('price', 'Prijs:') !!}
			{!! Form::number('price',null,['class'=>'form-control','step'=>'0.01']) !!}
		</div>
		<div class="form-group">
            {!! Form::label('promoted', 'Promoted:') !!}
			{!! Form::checkbox('promoted',null,null,['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('promoted_price', 'Sale prijs:') !!}
			{!! Form::number('promoted_price',null,['class'=>'form-control','step'=>'0.01']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('category', 'Category:') !!}
			{!! Form::select('category',$categories,$product->category_id, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
		</div>
		{!! Form::close() !!}
    </div>
</div>
@endsection
