@extends('layouts.backoffice')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Backoffice</a></li>
            <li><a href="{{ url('/user') }}">Users</a></li>
            <li class="active">User</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @if(null !== Session::get('error'))
        <div class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Er is iet fout!</strong> {{ Session::get('error') }}
        </div>
        @endif

        {!! Form::open(array('url' => 'user')) !!}
        <div class="form-group">
			{!! Form::label('name', 'Naam:') !!}
			{!! Form::text('name',null,['class'=>'form-control']) !!}
		</div>
        <div class="form-group">
			{!! Form::label('email', 'E-mail:') !!}
			{!! Form::email('email',null,['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
		</div>
		{!! Form::close() !!}
    </div>
</div>
@endsection
