@extends('layouts.backoffice')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Backoffice</a></li>
            <li class="active">Users</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @if ($users == null || count($users) < 1)
        <p>
            Er zijn nog geen gebruikers!
        </p>
        @else
        <p>
            <a href="{{ url('/user/create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Voeg een gebruiker toe</a>
        </p>
        <table class="table">
            <tbody>
                <tr>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Actions</th>
                </tr>
                @foreach ($users as $key => $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        {!! Form::open(['method' => 'DELETE','route' => ['user.destroy', $user->id], 'class' => 'form-delete']) !!}
                            <a href="{{ url('/user/'.$user->id.'/edit') }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-danger modal-button" data-product-id="{{ $user->id }}" data-toggle="modal" href="#modal-confirm"><i class="fa fa-trash-o"></i></a>
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
</div>

<!-- Modals -->
<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Verwijderen</h4>
            </div>
            <div class="modal-body">

                Ben je zeker dat je deze gebruiker wilt verwijderen?

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Terug</button>
                <button class="btn btn-warning modal-confirm-button" type="button"> Verwijderen</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
var activeForm;

$('.modal-button').click(handleModal);
$('.modal-confirm-button').click(handleConfirm);

function handleModal(e){
    activeForm = $(this).closest('.form-delete');
}

function handleConfirm(e){
    activeForm.submit();
}
</script>
@endsection
